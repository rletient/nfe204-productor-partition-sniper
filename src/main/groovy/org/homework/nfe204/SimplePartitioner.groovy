package org.homework.nfe204

import org.apache.kafka.clients.producer.Partitioner
import org.apache.kafka.common.Cluster
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class SimplePartitioner implements Partitioner {

    private final static Logger LOGGER = LoggerFactory.getLogger(SimplePartitioner.class)

    void configure(Map<String, ?> configs) {
        LOGGER.info("configure - " + configs)
    }

    int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
        LOGGER.info("partition - $topic - $key - $value - $cluster")
        return Integer.parseInt((String) key)
    }

    /**
     * This is called when partitioner is closed.
     */
    void close() {
        LOGGER.info("close")
    }

}
