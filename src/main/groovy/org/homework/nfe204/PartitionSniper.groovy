package org.homework.nfe204

import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.clients.producer.RecordMetadata
import org.apache.kafka.common.serialization.IntegerSerializer
import org.apache.kafka.common.serialization.StringSerializer

import java.util.concurrent.Future

// -k 172.18.0.3:9092,172.18.0.4:9092 -t test-consumer-topic -m "message de test" -p 1
class PartitionSniper {

    private String topic

    private KafkaProducer<String, String> producer

    private PartitionSniper() {}


    void emit(String partition, String message) {
        ProducerRecord<String, String> record = new ProducerRecord<>(topic, partition, message)
        Future<RecordMetadata> fRecordMedatadata = producer.send(record)
        RecordMetadata recordMetadata = fRecordMedatadata.get()
        println("Message($message) bien envoyé : Partition(${recordMetadata.partition()}) - Offset(${recordMetadata.offset()})")
    }

    static void main(String[] args) {
        def cli = new CliBuilder(usage: 'java -jar productor-partition-sniper-1.0-SNAPSHOT-shaded.jar -k 172.18.0.3:9092,172.18.0.4:9092 -t test-consumer-topic -m "message de test" -p 0')
        cli.with {
            h longOpt: 'help', 'Show usage information'
            m longOpt: 'message', required: true, args: 1, argName: 'message', 'Message à envoyer'
            k longOpt: 'kafkaBrokers', required: true, args: 1, argName: 'kafkaBrokers', 'Liste des urls des noeuds Kafka'
            t longOpt: 'topic', required: true, args: 1, argName: 'topic', 'Nom du topic cible'
            p longOpt: 'partition', required: true, args: 1, argName: 'partition', 'Partition cible'
        }
        def options = cli.parse(args)
        if (!options) {
            return
        }
        if (options.h) {
            cli.usage()
        }
        Properties properties = new Properties()
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, options.k)
        properties.put(ProducerConfig.CLIENT_ID_CONFIG, "PartitionSniper")
        properties.put("partitioner.class", "org.homework.nfe204.SimplePartitioner")
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName())
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName())
        PartitionSniper emitter = new PartitionSniper()
        emitter.topic = options.t
        emitter.producer = new KafkaProducer<>(properties)
        emitter.emit(options.p, options.m)
    }
}
